//import Amplify, { API, graphqlOperation } from "aws-amplify"
global.WebSocket = require('ws');
const Amplify = require('aws-amplify');

// Dev-Us
const aws_config = {
    'aws_appsync_graphqlEndpoint': 'https://tzcfvitrofdqhalfqppdsmt3eu.appsync-api.us-east-1.amazonaws.com/graphql',
    'aws_appsync_region': 'us-east-1',
    'aws_appsync_authenticationType': 'AWS_LAMBDA',
}

// Playground
/*const aws_config = {
  'aws_appsync_graphqlEndpoint': 'https://uxkzitkotbg4jkmktmmltkem2u.appsync-api.us-east-1.amazonaws.com/graphql',
  'aws_appsync_region': 'us-east-1',
  'aws_appsync_authenticationType': 'AWS_LAMBDA',
}*/

Amplify.API.configure(aws_config)

const ticket = '';
const orgId = '2129629959';
const authToken = `hey-appsync type=organization orgid=${2129629959} suh=ro ticket=${ticket}`;

const subcribeToAgentModified = `subscription onAgentModified($orgId: String!) {
    onAgentModified(orgId: $orgId) {
        orgId
    }
}`;

const subscribeOp = Amplify.graphqlOperation(
    subcribeToAgentModified,
    { orgId: orgId },
    authToken,
);

const subscription = Amplify.API.graphql(
    subscribeOp
  ).subscribe({
      next: (eventData) => console.log(`Received notification : ${JSON.stringify(eventData.value.data)}`),
      error: error => console.log(`Error : ${JSON.stringify(error.error)}`),
  });

const pressAnyKey = require('press-any-key');
pressAnyKey("Press any key to resolve, or CTRL+C to reject\n", {
  ctrlC: "reject"
})
  .then(() => {
    subscription.unsubscribe();
    console.log('You pressed any key')
    process.exit();
  })
  .catch(() => {
    console.log('You pressed CTRL+C')
  })